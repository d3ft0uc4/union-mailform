# coding=utf-8

BASE_URL = 'https://podshipnik-mo.bpmonline.com/'

operation_types = {
    "Select": 0,
    "Insert": 1,
    "Update": 2,
    "Delete": 3,
    "Batch": 4
}
expression_types = {
    "SchemaColumn": 0,
    "Function": 1,
    "Parameter": 2,
    "SubQuery": 3,
    "ArithmeticOperation": 4
}
data_value_types = {
    "Guid": 0,
    "Text": 1,
    "Integer": 4,
    "Float": 5,
    "Money": 6,
    "DateTime": 7,
    "Date": 8,
    "Time": 9,
    "Lookup": 10,
    "Enum": 11,
    "Boolean": 12,
    "Blob": 13,
    "Image": 14,
    "ImageLookup": 16,
    "Color": 18,
    "Mapping": 26
}

credentials = {
    'UserName': 'Supervisor',
    'UserPassword': 'Supervisor'
}

headers = {'Content-Type': 'application/json'}
base_view = '{0}/0/Nui/ViewModule.aspx'.format(BASE_URL)
auth_url = '{0}/ServiceModel/AuthService.svc/Login'.format(BASE_URL)

ops = ['SELECT', 'INSERT', 'UPDATE', 'DELETE']
endpoints = {k: '{0}/0/DataService/json/reply/{1}Query'.format(BASE_URL, k.capitalize()) for k in ops}


def select_all(entity):
    return {
        "RootSchemaName": entity,
        "OperationType": operation_types['Select'],
        "AllColumns": True,
        "Columns": {
            "Items": {
            }
        }
    }


def select_by_column(entity, col_name, col_value, columns=['Id']):
    obj = {
        "RootSchemaName": entity,
        "OperationType": operation_types['Select'],
        "AllColumns": False,
        "Columns": {
            "Items": {}
        },
        "filters": {
            "items": {
                "primaryColumnFilter": {
                    "filterType": 1,
                    "comparisonType": 3,
                    "isEnabled": True,
                    "trimDateTimeParameterToDate": False,
                    "leftExpression": {"expressionType": 0, "columnPath": col_name},
                    "rightExpression": {
                        "expressionType": expression_types['Parameter'],
                        "parameter": {
                            "dataValueType": 0,
                            "value": col_value
                        }
                    }
                }
            },
            "logicalOperation": 0,
            "isEnabled": True,
            "filterType": 6
        }
    }
    for col in columns:
        obj['Columns']['Items'][col] = {"caption": "", "orderDirection": 0, "orderPosition": -1, "isVisible": True,
                                        "expression": {"expressionType": 0, "columnPath": col}
                                        }
    return obj


def delete_by_column(entity, col_name, col_value):
    return {
        "RootSchemaName": entity,
        "OperationType": operation_types['Delete'],
        "filters": {
            "items": {
                "primaryColumnFilter": {
                    "filterType": 1,
                    "comparisonType": 3,
                    "isEnabled": True,
                    "trimDateTimeParameterToDate": False,
                    "leftExpression": {"expressionType": 0, "columnPath": col_name},
                    "rightExpression": {
                        "expressionType": expression_types['Parameter'],
                        "parameter": {
                            "dataValueType": 0,
                            "value": col_value
                        }
                    }
                }
            },
            "logicalOperation": 0,
            "isEnabled": True,
            "filterType": 6
        }
    }
