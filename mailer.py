# coding=utf-8
import datetime
import hashlib
import logging
import os
import requests
from bs4 import BeautifulSoup
from exchangelib import Account, Credentials
from helper_classes import *

creds = {
    'mail': 'unionindustrials@podshipnik-mo.ru',
    'pwd': 'L3Vg4U(Gsy'
}
mail_cr = Credentials(creds['mail'], creds['pwd'])
account = Account(creds['mail'], credentials=mail_cr, autodiscover=True)
log_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), '{0}.log')
logging.basicConfig(filename=log_file.format(str(datetime.date.today())),
                    format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%d-%m-%Y:%H:%M:%S')
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log.info("Current time:{0}".format(datetime.datetime.now()))

csrf = None

# Matcher criteria -> LandingType
search_criteria = [
    u'Заполнена анкета на Вашем сайте "Вам перезвонить?" на странице:',
    u'На Вашем сайте podshipnik-mo.ru была заполнена форма обратной связи сервиса',
    u'Заполнена анкета на Вашем сайте "Получить прайс-лист" на странице:',
    u'Заказ в интернет-магазине # ',
    u'Заявка на бесплатную консультацию (ISB)',
    u'Заявка на запрос каталога ',
    u'Заявка на расчет стоимости (ISB)',
    u'Заполнена анкета на Вашем сайте "Онлайн-заявка" на странице:',
    u'fbj.podshipnik-mo.ru',
    u'На сайте http://podshipnik-mo.ru зарегистрировался новый пользователь'
]

landings = {
    'ВАМ ПЕРЕЗВОНИТЬ? PODSHIPNIK-MO.RU': '3690d9f1-91f8-4cc8-99b3-286fe6480f59',
    'ЗАКАЗ В ИНТЕРНЕТ-МАГАЗИНЕ': '633734a1-ddf6-4cc0-9c2e-e18630922f88',
    'ЗАЯВКА НА БЕСПЛАТНУЮ КОНСУЛЬТАЦИЮ (ISB)': '8fa753d2-5ae5-4e14-bbfc-6150ea3d49fa',
    'ЗАЯВКА НА ЗАПРОС КАТАЛОГА "ПОДШИПНИКИ И КОМПОНЕНТЫ " (ISB)': 'ceffa561-64d4-4d59-9f1a-1282d60df274',
    'ЗАЯВКА НА РАСЧЕТ СТОИМОСТИ (ISB)': '6a22f215-a831-4531-b320-6a2cd21e3f8c',
    'НОВЫЙ ПОЛЬЗОВАТЕЛЬ НА САЙТЕ PODSHIPNIK-MO.RU': '56968a8d-579d-47cb-b1f1-d6b9c25659b5',
    'ОНЛАЙН-ЗАЯВКА PODSHIPNIK-MO.RU': '18498cab-7b6a-4556-b88a-9ab656d1a529',
    'ОФФЛАЙН СООБЩЕНИЕ С ВАШЕГО САЙТА PODSHIPNIK-MO.RU': 'd9a19972-0883-47da-b3bb-7adcee4ceb4b',
    'ПОЛУЧИТЬ ПРАЙС-ЛИСТ PODSHIPNIK-MO.RU': '26b59998-8175-4e43-a85d-edcc836c8ea3',
    'Заявка на полный прайс-лист (FBJ)': '91bea89b-ea9b-425b-bd72-937ecfe94dd5',
    'ЗАЯВКА НА РАСЧЁТ СТОИМОСТИ (FBJ)': '65338ccb-a476-4711-a42e-3a0b3d1a68d0'
}


def SELECT(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['SELECT'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = ses.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            SELECT(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def INSERT(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['INSERT'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = ses.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            INSERT(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def UPDATE(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['UPDATE'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = ses.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            UPDATE(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def DELETE(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['DELETE'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = ses.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            DELETE(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def hash(raw_str):
    return hashlib.sha1(raw_str.encode('utf-8')).hexdigest()


# 4 flds -> [0,1,2,3]
def parse_html_form(raw_str):
    soup = BeautifulSoup(raw_str, 'html.parser')
    d = {}
    for i, el in enumerate(soup.find_all('li')):
        if i == 0:
            d['Commentary'] = el.get_text().replace(u'Что Вас интересует?: ', u'')
        elif i == 1:
            d['Name'] = el.get_text().replace(u'Ваше имя:: ', u'')
        elif i == 2:
            term = el.get_text().replace(u'Ваш e-mail или телефон:: ', u'')
            if u'@' in term:
                d['Email'] = term
            else:
                d['MobilePhone'] = term
        elif i == 3:
            pass
    d['UsrEmailID'] = hash(raw_str)
    d['WebForm'] = 'd9a19972-0883-47da-b3bb-7adcee4ceb4b'
    return d


def parse_html_form2(raw_str):
    soup = BeautifulSoup(raw_str, 'html.parser')
    d = {}
    for i, el in enumerate(soup.find_all('div')):
        if i == 1:
            d['Name'] = el.get_text()
        elif i == 3:
            d['MobilePhone'] = el.get_text()
        elif i == 5:
            d['Email'] = el.get_text()
        elif i == 7:
            d['Commentary'] = el.get_text()
    d['UsrEmailID'] = hash(raw_str)
    d['WebForm'] = '18498cab-7b6a-4556-b88a-9ab656d1a529'
    return d


def copied_message(body):
    if u'Original Message' not in body and u'Пересылаемое сообщение' not in body:
        return False
    return True


# Getting mails
def get_leads():
    parsed_mails = []
    for criteria in search_criteria:

        if criteria == u'Заполнена анкета на Вашем сайте "Вам перезвонить?" на странице:':
            res = account.inbox.all().filter(
                body__contains=criteria)
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    d = {
                        'Name': terms[2].replace(u'Имя: ', u''),
                        'MobilePhone': terms[3].replace(u'Контакты: ', u''),
                        'WebForm': '3690d9f1-91f8-4cc8-99b3-286fe6480f59',
                        'UsrEmailID': hash(item.body)
                    }
                    parsed_mails.append(d)
        elif criteria == u'Заполнена анкета на Вашем сайте "Онлайн-заявка" на странице:':
            res = account.inbox.all().filter(subject__contains=u'Онлайн-заявка podshipnik-mo.ru')
            for item in res:
                if not copied_message(item.body):
                    d = parse_html_form2(item.body)
                    parsed_mails.append(d)
        elif criteria == u'На Вашем сайте podshipnik-mo.ru была заполнена форма обратной связи сервиса':
            res = account.inbox.all().filter(
                subject__contains=u'Оффлайн сообщение с Вашего сайта podshipnik-mo.ru')
            for item in res:
                if not copied_message(item.body):
                    parsed_mails.append(parse_html_form(item.body))

        elif criteria == u'Заказ в интернет-магазине # ':
            res = account.inbox.all().filter(
                subject__contains=criteria)
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    d = {
                        'Name': terms[0].replace(u'Плательщик: ', u''),
                        'MobilePhone': terms[1].replace(u'Телефон: ', u''),
                        'Email': terms[2].replace(u'E-mail: ', u''),
                        'WebForm': '633734a1-ddf6-4cc0-9c2e-e18630922f88',
                        'UsrEmailID': hash(item.body),
                        'Commentary': '\n'.join(terms[3:])
                    }
                    parsed_mails.append(d)
        elif criteria == u'Заполнена анкета на Вашем сайте "Получить прайс-лист" на странице:':
            res = account.inbox.all().filter(
                body__contains=criteria)
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    d = {
                        'Name': terms[2].replace(u'E-mail: ', u''),
                        'MobilePhone': terms[3].replace(u'Ваш телефон: ', u''),
                        'Email': terms[2].replace(u'E-mail: ', u''),
                        'WebForm': '26b59998-8175-4e43-a85d-edcc836c8ea3',
                        'UsrEmailID': hash(item.body)
                    }
                    parsed_mails.append(d)

        elif criteria == u'Заявка на бесплатную консультацию (ISB)':
            res = account.inbox.all().filter(
                subject__contains=criteria)
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    d = {
                        'MobilePhone': terms[2].replace(u'Телефон клиента : ', u''),
                        'Name': terms[3].replace(u'Имя клиента : ', u''),
                        'Email': terms[4].replace(u'E-mail клиента : ', u''),
                        'WebForm': '8fa753d2-5ae5-4e14-bbfc-6150ea3d49fa',
                        'UsrEmailID': hash(item.body)
                    }
                    parsed_mails.append(d)
        elif criteria == u'Заявка на запрос каталога ':
            res = account.inbox.all().filter(
                subject__contains=criteria)
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    d = {
                        'MobilePhone': terms[2].replace(u'Телефон клиента : ', u''),
                        'Name': item.subject + '/' + terms[3].replace(u'Имя клиента : ', u''),
                        'Email': terms[4].replace(u'E-mail клиента : ', u''),
                        'WebForm': 'ceffa561-64d4-4d59-9f1a-1282d60df274',
                        'UsrEmailID': hash(item.body)
                    }
                    parsed_mails.append(d)
        elif criteria == u'Заявка на расчет стоимости (ISB)':
            res = account.inbox.all().filter(
                subject__contains=criteria)
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    d = {
                        'MobilePhone': terms[2].replace(u'Телефон клиента : ', u''),
                        'Name': terms[3].replace(u'Имя клиента : ', u''),
                        'Email': terms[4].replace(u'E-mail клиента : ', u''),
                        'WebForm': '6a22f215-a831-4531-b320-6a2cd21e3f8c',
                        'UsrEmailID': hash(item.body)
                    }
                    parsed_mails.append(d)
        elif criteria == u'fbj.podshipnik-mo.ru':
            res = account.inbox.all().filter(
                display_to=u'noreply@fbj-bearing.ru')
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    if item.subject == u'Заявка на полный прайс-лист':
                        d = {
                            'MobilePhone': terms[3].replace(u'Телефон клиента : ', u''),
                            'Name': terms[4].replace(u'Имя клиента : ', u''),
                            'Email': terms[5].replace(u'E-mail клиента : ', u''),
                            'WebForm': '91bea89b-ea9b-425b-bd72-937ecfe94dd5',
                            'UsrEmailID': hash(item.body)
                        }
                        parsed_mails.append(d)
                    elif item.subject == u'Заявка на расчет стоимости':
                        d = {
                            'MobilePhone': terms[3].replace(u'Телефон клиента : ', u''),
                            'Name': terms[4].replace(u'Имя клиента : ', u''),
                            'Email': terms[5].replace(u'E-mail клиента : ', u''),
                            'WebForm': '65338ccb-a476-4711-a42e-3a0b3d1a68d0',
                            'UsrEmailID': hash(item.body)
                        }
                        parsed_mails.append(d)
        elif criteria == u'На сайте http://podshipnik-mo.ru зарегистрировался новый пользователь':
            res = account.inbox.all().filter(
                body__contains=u'зарегистрировался новый пользователь')
            for item in res:
                if not copied_message(item.body):
                    terms = item.body.split('\n')
                    d = {
                        'Name': terms[1].split(u'Имя: '),
                        'Email': terms[0].split(u'E-mail: ')[-1],
                        'WebForm': '56968a8d-579d-47cb-b1f1-d6b9c25659b5',
                        'UsrEmailID': hash(item.body)
                    }
                    parsed_mails.append(d)

    leads = map(create_lead, parsed_mails)
    return leads


# Имя
# Мобильный телефон
# Email
# Комментарий
# ID Email
# Landing
def create_lead(d):
    _lead = Lead()
    _lead.Contact = Text(d.get('Name', u''))
    _lead.Commentary = Text(d.get('Commentary', u''))
    _lead.Email = Text(d.get('Email', u''))
    _lead.MobilePhone = Text(d.get('MobilePhone', u''))
    _lead.UsrEmailID = Text(d.get('UsrEmailID', u''))
    _lead.WebForm = Lookup(d.get('WebForm', u''))
    _lead.RegisterMethod = Lookup('ba097c3a-31cf-48a7-a196-84fad50efe8d')
    return _lead


def insert_to_bpm(lead, session, csrf):
    tryExist = json.loads(SELECT(session, select_by_column('Lead', 'UsrEmailID', lead.UsrEmailID)))['rows']
    isExists = bool(len(tryExist))
    if not isExists:
        req = json.loads(lead.toJSON(), encoding='windows-1251')
        log.debug(INSERT(session, req))
        log.info('Inserted lead:{}'.format(lead.toJSON()))


def get_auth():
    _session = requests.Session()
    auth = _session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
    if json.loads(auth.text)['Code'] == 0:
        log.info('Login OK...')
        _csrf = _session.cookies.get_dict().get('BPMCSRF', None)
        return _session, _csrf
    else:
        raise Exception('Auth error:\n{}'.format(auth.text))


if __name__ == '__main__':
    log.info('Starting parser..')
    (session, csrf) = get_auth()
    leads = get_leads()
    for lead in leads:
        insert_to_bpm(lead, session, csrf)
    log.info('Finishing parser..')
