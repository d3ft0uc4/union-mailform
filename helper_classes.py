import json
from constants import *


class Serializable:
    def __init__(self, **kwargs):
        self.__dict__ = kwargs

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4, encoding='windows-1251')


class Parameter(Serializable):
    def __init__(self, value):
        Serializable.__init__(self)
        self.expressionType = expression_types['Parameter']
        self.parameter = Serializable()
        self.parameter.value = value


class Guid(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Guid']


class Text(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Text']


class Integer(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, int(value))
        self.parameter.dataValueType = data_value_types['Integer']


class Float(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, float(value))
        self.parameter.dataValueType = data_value_types['Float']


class Money(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Money']


class DateTime(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['DateTime']


class Date(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Date']
        self.parameter.value = value


class Time(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Time']


class Lookup(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Lookup']


class Enum(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Enum']


class Boolean(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, bool(value))
        self.parameter.dataValueType = data_value_types['Boolean']


class Blob(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Blob']


class Image(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Image']


class ImageLookup(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Blob']


class Color(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Color']


class Mapping(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Mapping']


class BasicObject(Serializable):
    def __init__(self, operationType=operation_types['Insert']):
        Serializable.__init__(self)
        self.rootSchemaName = None
        self.columnValues = Serializable()
        self.columnValues.items = Serializable()
        self.operationType = operationType

    def __getattr__(self, item):
        if item == 'items':
            return self.columnValues.__dict__[item]
        elif item not in (['rootSchemaName', 'columnValues', 'operationType', 'filters', 'columns']):
            return self.columnValues.items.__dict__[item].parameter.value
        else:
            return self.__dict__[item]

    def __setattr__(self, key, value):
        if key == 'items':
            self.columnValues[key] = value
        elif key not in (['rootSchemaName', 'columnValues', 'operationType', 'filters', 'columns']):
            self.columnValues.items.__dict__[key] = value
        else:
            self.__dict__[key] = value


class Lead(BasicObject):
    def __init__(self, operationType=operation_types['Insert']):
        BasicObject.__init__(self, operationType)
        self.rootSchemaName = "Lead"


class IdFilter(Serializable):
    def __init__(self, _id):
        Serializable.__init__(self)
        self.items = Serializable(primaryColumnFilter=
        Serializable(
            filterType=1,
            comparisonType=3,
            isEnabled=True,
            trimDateTimeParameterToDate=False,
            leftExpression=Serializable(
                expressionType=1,
                functionType=1,
                macrosType=34
            ),
            rightExpression=Serializable(
                expressionType=2,
                parameter=Serializable(
                    dataValueType=0,
                    value=_id
                )
            )
        ))
        self.logicalOperation = 0
        self.isEnabled = True
        self.filterType = 6


def to_update(obj, original_id):
    obj.operationType = operation_types['Update']
    obj.__dict__['columnValues'].__dict__['items'].__dict__.pop('Id', None)
    obj.__dict__['isForceUpdate'] = True
    obj.filters = IdFilter(original_id)
    return obj
